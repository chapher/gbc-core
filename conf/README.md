# Configure Files for GameBox Cloud Core

-   redis.conf
    used for redis server.

-   nginx.conf
    default configure file for nginx in GameBox Cloud Core.

-   nginx.conf.optimized
    an example of nginx.conf optimized.

-   supervisord.conf / supervisord.conf.d
    supervisor conf file for GameBox Cloud Core.
